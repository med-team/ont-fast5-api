Source: ont-fast5-api
Section: science
Priority: optional
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Nilesh Patra <nilesh@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3-all-dev,
               python3-setuptools,
               python3-h5py,
               python3-numpy,
               python3-progressbar,
               python3-packaging,
               libvbz-hdf-plugin-dev
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/ont-fast5-api
Vcs-Git: https://salsa.debian.org/med-team/ont-fast5-api.git
Homepage: https://github.com/nanoporetech/ont_fast5_api/
Rules-Requires-Root: no

Package: ont-fast5-api
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${python3:Depends},
         python3-progressbar,
         libvbz-hdf-plugin-dev,
         python3-h5py
Description: simple interface to HDF5 files of the Oxford Nanopore .fast5 file format
 Ont_fast5_api is a simple interface to HDF5 files of the Oxford
 Nanopore .fast5 file format.
 .
 It provides:
 .
  * Concrete implementation of the fast5 file schema using the generic h5py
    library
  * Plain-english-named methods to interact with and reflect the fast5 file
    schema
  * Tools to convert between multi_read and single_read formats
  * Tools to compress/decompress raw data in files
